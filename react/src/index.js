import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './webapp/index.scss';
import * as serviceWorker from './webapp/serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import UploadContent from './webapp/components/UploadContent/UploadContent';
import Page from './webapp/components/Page/Page';
import LoginContent from './webapp/components/LoginContent/LoginContent';
import RegisterContent from './webapp/components/RegisterContent/RegisterContent';
import CrawlerContent from './webapp/components/CrawlerContent/CrawlerContent';
import SearchContent from './webapp/components/SearchContent/SearchContent';
import Notify from './webapp/components/api/notification-service';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import userContext, { INITIAL } from './webapp/components/contexts/userContext';
import CrawlerHistoryContent from './webapp/components/CrawlerHistoryContent/CrawlerHistoryContent';

class App extends Component {
  componentDidMount() {
    Notify.notifications.subscribe((alert) => alert instanceof Function && alert());
  }

  render = () => (
    <>
      <React.StrictMode>
        <userContext.Provider value={INITIAL}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/">
                <Page children={<LoginContent />} />
              </Route>
              <Route path="/register">
                <Page children={<RegisterContent />} />
              </Route>
              <Route path="/upload">
                <Page children={<UploadContent />} />
              </Route>
              <Route path="/crawler">
                <Page children={<CrawlerContent />} />
              </Route>
              <Route path="/crawler-history">
                <Page children={<CrawlerHistoryContent />} />
              </Route>
              <Route path="/search">
                <Page children={<SearchContent />} />
              </Route>
            </Switch>
          </BrowserRouter>
        </userContext.Provider>
      </React.StrictMode>
      <ToastContainer autoClose={3500} />
    </>
  );
}
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
