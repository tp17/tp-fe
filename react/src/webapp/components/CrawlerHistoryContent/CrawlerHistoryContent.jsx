import './CrawlerHistoryContent.scss';
import React, { useEffect, useState } from 'react';
import DefaultTable from '../DefaultTable/DefaultTable';
import { CrawlerService } from '../api/crawler-service';
import CardWrapper from '../CardWrapper/CardWrapper';
import { ListGroup } from 'react-bootstrap';

const LogDetail = ({ data }) => {
  const { logs } = data;
  return (
    <ListGroup variant="flush" className="log-list">
      {logs.map((log, key) => (
        <ListGroup.Item className="log-list-item" key={key}>
          <div style={{ width: '100px', fontWeight: 'bold' }}>{log.split('-')[0]}</div>
          <span>&emsp;</span>
          <span>{log.split('-')[1]}</span>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
};

const CrawlerHistoryContent = () => {
  let [crawledLogs, setCrawlerLogs] = useState([]);

  useEffect(() => {
    CrawlerService.getCrawled(localStorage.getItem('username')).then(({ data }) =>
      setCrawlerLogs(data)
    );
    const interval = setInterval(() => {
      CrawlerService.getCrawled(localStorage.getItem('username')).then(({ data }) =>
        setCrawlerLogs(data)
      );
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const tableConfig = {
    striped: true,
    dense: true,
    expandableRows: true,
    highlightOnHover: true,
    expandableComponent: <LogDetail />,
    columns: [
      {
        name: 'Root',
        selector: 'root',
        sortable: true,
      },
      {
        name: 'Started',
        selector: 'started_at',
        sortable: true,
      },
      {
        name: 'Logs',
        selector: 'logs',
        sortable: true,
        format: (row) => row.logs.length,
      },
      {
        name: 'Downloaded',
        selector: 'downloaded',
        sortable: true,
        format: (row) => row.logs.filter((log) => log.startsWith('[DOWNLOADING]')).length,
      },
    ],
  };

  return (
    <CardWrapper
      className="enlarged"
      title="Crawled History"
      children={<DefaultTable config={tableConfig} data={crawledLogs} />}
    />
  );
};

export default CrawlerHistoryContent;
