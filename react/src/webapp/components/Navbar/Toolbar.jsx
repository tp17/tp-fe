import './Toolbar.scss';
import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import MalwareIcon from '../../static/icons/MalwareIcon';
import { NavLink } from 'react-router-dom';
import { AuthService } from '../api/auth-service';
import userContext from '../contexts/userContext';
import { useHistory } from 'react-router-dom';

const Toolbar = () => {
  let user = useContext(userContext);
  const history = useHistory();
  const handleLogout = () => {
    AuthService.logout().then();
    user.userName = '';
    user.userAPI = '';
    user.loggedIn = false;
    history.push('/');
  };

  return (
    <Navbar bg="light" className="toolbar shadow" fixed="top">
      <Navbar.Brand className="navbar-title">
        <NavLink exact to="/">
          <MalwareIcon />
          <div className="logo" />
        </NavLink>
      </Navbar.Brand>
      {!user.loggedIn && (
        <Nav className="mr-auto">
          <div className="Links">
            <NavLink to="/register">Register</NavLink>
          </div>
        </Nav>
      )}
      {user.loggedIn && (
        <Nav className="mr-auto">
          <div className="Links">
            <NavLink to="/upload">Upload</NavLink>
          </div>
          <div className="Links">
            <NavLink to="/search">Search</NavLink>
          </div>
          <div className="Links">
            <NavLink to="/crawler">Crawl</NavLink>
          </div>
          <div className="Links">
            <NavLink to="/crawler-history">Crawl-History</NavLink>
          </div>
        </Nav>
      )}
      <Nav className="ml-auto">
        {user.loggedIn && (
          <div className="Links" onClick={(e) => handleLogout(e)}>
            <NavLink to="">Logout</NavLink>
          </div>
        )}
      </Nav>
    </Navbar>
  );
};

export default Toolbar;
