import './LoginContent.scss';
import React, { useContext, useRef } from 'react';
import { NavLink } from 'react-router-dom';
import CardWrapper from '../CardWrapper/CardWrapper';
import { Button, Form } from 'react-bootstrap';
import { AuthService } from '../api/auth-service';
import { useHistory } from 'react-router-dom';
import userContext from '../contexts/userContext';

export const LoginForm = () => {
  const history = useHistory();
  const user = useContext(userContext);
  const form = useRef(null);
  const handleLogin = async (e) => {
    e.preventDefault();
    const data = new FormData(form.current);
    AuthService.login(data).then((res) => {
      if (res.data.success) {
        user.userName = res.data.user.username;
        user.userAPI = localStorage.getItem('key');
        user.loggedIn = true;
        history.push('/upload');
      }
    });
  };

  return (
    <Form ref={form} onSubmit={(e) => handleLogin(e)}>
      <Form.Group controlId="username">
        <Form.Label>Username</Form.Label>
        <Form.Control type="text" placeholder="John Doe" name="username" required />
      </Form.Group>
      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" name="password" required />
      </Form.Group>
      <Form.Group className="form-btn-group">
        <Button className="form-btn" type="submit">
          Sign in
        </Button>
        <NavLink to="/register" className="form-link">
          Register
        </NavLink>
      </Form.Group>
    </Form>
  );
};

const LoginContent = () => {
  return <CardWrapper title="Login" children={<LoginForm />} />;
};

export default LoginContent;
