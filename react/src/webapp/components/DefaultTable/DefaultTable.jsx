import React, { useState } from 'react';
import DataTable from 'react-data-table-component';

const DefaultTable = ({ config, data }) => {
  const [perPage, setPerPage] = useState(config.perPage || 10);

  const handlePerPageChange = (newPerPage) => {
    setPerPage(newPerPage);
  };

  const handleRowClick = (event) => {
    if (config.onClick && typeof config.onClick === 'function') {
      return config.onClick(event);
    }
  };

  return (
    <DataTable
      noHeader={!config.title}
      title={config.title}
      columns={config.columns}
      data={data}
      pagination={data.length > 10}
      onChangeRowsPerPage={handlePerPageChange}
      dense={config.dense}
      striped={config.striped}
      selectableRows={config.selectable}
      onSelectedRowsChange={config.onSelect}
      onRowClicked={handleRowClick}
      expandableRows={config.expandableRows}
      highlightOnHover={config.highlightOnHover}
      expandableRowsComponent={config.expandableComponent}
      expandableRowExpanded={config.expandableRowExpanded}
      onRowExpandToggled={config.onRowExpandToggled}
      expandOnRowClicked
      expandableRowsHideExpander
    />
  );
};

export default DefaultTable;
