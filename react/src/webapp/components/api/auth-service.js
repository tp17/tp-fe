import Notify, { AlertTypes } from './notification-service';
import axios from 'axios';

export const baseURL = process.env.REACT_APP_API_URL;

export const AuthService = {
  register: (data) => {
    axios
      .post(`${baseURL}/register`, data)
      .then((response) => {
        console.log('response from service: ', response);
        if (response.data.success) {
          console.log('Success: ', response.data.message);
          Notify.sendNotification(response.data.message, AlertTypes.success);
        } else {
          console.log('Failed: ', response.data.message);
          Notify.sendNotification(response.data.message, AlertTypes.error);
        }
      })
      .catch((error) => {
        console.error('error response: ', error);
        Notify.sendNotification('Registration failed', AlertTypes.error);
      });
  },

  login: (data) => {
    return axios
      .post(`${baseURL}/login`, data)
      .then((response) => {
        console.log('response from service: ', response);
        if (response.data.success) {
          console.log('Success: ', response.data);
          Notify.sendNotification(response.data.message, AlertTypes.success);
          localStorage.setItem('key', response.data.user.apiKey);
          localStorage.setItem('username', response.data.user.username);
        } else {
          console.log('Failed: ', response.data.message);
          Notify.sendNotification(response.data.message, AlertTypes.error);
        }
        return response;
      })
      .catch((error) => {
        console.error('error response: ', error);
        Notify.sendNotification('Login failed', AlertTypes.error);
      });
  },

  logout: () => {
    return axios
      .post(`${baseURL}/logout`)
      .then(() => {
        Notify.sendNotification('Logged out successfully', AlertTypes.success);
        localStorage.clear();
        console.log('local storage cleared.');
      })
      .catch((error) => {
        console.error('error response: ', error);
        Notify.sendNotification('Logout failed', AlertTypes.error);
      });
  },
};
