import axios from 'axios';
import Notify, { AlertTypes } from './notification-service';

export const apiClient = axios.create();

apiClient.interceptors.request.use((config) => {
  const apiKey = localStorage.getItem('key');
  if (apiKey == null) {
    return Promise.reject('Missing API key');
  }
  config.params = config.params || {};
  config.params['key'] = apiKey;
  return config;
});

apiClient.defaults.baseURL = process.env.REACT_APP_API_URL;

apiClient.interceptors.response.use(
  (data) => data,
  (error) => Notify.sendNotification(error, AlertTypes.error)
);
