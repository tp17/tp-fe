import { apiClient } from './api-client';

export const CrawlerService = {
  crawl: (root, max) => {
    return apiClient
      .post(`/crawl?max=${max}`, { root })
      .then((response) => console.log('RESPONSE', response));
  },

  getCrawled: (user) => {
    return apiClient.get(`/crawled?for=${user}`);
  },
};
