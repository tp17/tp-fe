import { apiClient } from './api-client';

const defaultConfig = {
  headers: {
    'Content-Type': 'multipart/form-data',
    'Access-Control-Allow-Origin': '*',
  },
};
export const FileService = {
  uploadFile: (formData, scan) => {
    const config = { ...defaultConfig, params: { scan } };

    return apiClient
      .post('/upload', formData, config)
      .then((res) => res.data)
      .catch((err) => console.warn(err));
  },
  getFilesFromDatabase: (setFiles) => {
    return apiClient
      .get('/findall', defaultConfig)
      .then((res) => setFiles(res.data))
      .catch((e) => console.log(e));
  },
  uploadFilesToZip: async (ArrayIds) => {
    return await apiClient.post('/createzip', ArrayIds, {
      ...defaultConfig,
      ...{ responseType: 'arraybuffer' },
    });
  },
};
