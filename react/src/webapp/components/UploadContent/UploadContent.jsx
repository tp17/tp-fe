import './UploadContent.scss';
import '../LoginContent/LoginContent.scss';
import { io } from 'socket.io-client';
import React, { useContext, useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { FileService } from '../api/routes';
import UploadTable from '../UploadTable/UploadTable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import CardWrapper from '../CardWrapper/CardWrapper';
import { useHistory } from 'react-router-dom';
import userContext from '../contexts/userContext';
import Notify, { AlertTypes } from '../api/notification-service';

const UploadForm = () => {
  let [file, setFile] = useState('');
  let [uploads, setUploads] = useState([]);
  let [fileName, setFileName] = useState('Choose a file');
  let [scanChecked, setScanChecked] = useState(true);
  const pollTime = 1000; // 1 sec
  const history = useHistory();
  const user = useContext(userContext);

  useEffect(() => {
    const socket = io('http://localhost:5000');

    socket.on('message', (data) => setUploads(data['uploads']));
    const interval = setInterval(() => socket.emit('get_running_tasks'), pollTime);

    return () => clearInterval(interval);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (file) {
      let formData = new FormData();
      formData.append('file', file);
      FileService.uploadFile(formData, scanChecked).then((data) => {
        console.log(data);
        if (data.length === 0 || (data.length === 1 && data[0] === 0)) {
          Notify.sendNotification(
            "No files were saved, make sure your zipfile is not empty and doesn't contain folders",
            AlertTypes.warn
          );
        } else {
          Notify.sendNotification(
            `${data.length} file${data.length > 1 ? 's' : ''} successfully saved`,
            AlertTypes.success
          );
        }
      });
    }
  };

  const onFileChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      setFileName(file.name);
      setFile(file);
    }
  };
  if (!user.loggedIn) {
    history.push('/');
  }

  return (
    <>
      {uploads.length > 0 && <UploadTable data={uploads} />}
      <Form method="post" encType="multipart/form-data">
        <Form.Group className="form-btn-group">
          <input
            type="file"
            id="file"
            className="input-file"
            onChange={onFileChange}
            data-multiple-caption="{count} files selected"
            multiple
          />
          <label htmlFor="file">
            <FontAwesomeIcon icon={faUpload} className="fa-icon" />
            {fileName}
          </label>
          <Form.Check
            className="form-checkbox"
            type="checkbox"
            label="Scan"
            checked={scanChecked}
            onChange={() => setScanChecked(!scanChecked)}
          />
          <Button type="submit" className="form-btn" onClick={(e) => handleSubmit(e)}>
            Upload
          </Button>
        </Form.Group>
      </Form>
    </>
  );
};

const UploadContent = () => {
  return <CardWrapper title="Upload" children={<UploadForm />} />;
};

export default UploadContent;
