import './RegisterContent.scss';
import React, { useRef } from 'react';
import { Button, Form } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import CardWrapper from '../CardWrapper/CardWrapper';
import { AuthService } from '../api/auth-service';
import Notify, { AlertTypes } from '../api/notification-service';

export const RegisterForm = () => {
  const form = useRef(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(form.current);
    if (data.get('password') !== data.get('confirm-password')) {
      return Notify.sendNotification('Password does not match', AlertTypes.error);
    }
    AuthService.register(data);
  };

  return (
    <Form onSubmit={handleSubmit} ref={form}>
      <Form.Group controlId="email">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="JohnDoe@gmail.com" name="email" required />
      </Form.Group>
      <Form.Group controlId="username">
        <Form.Label>Username</Form.Label>
        <Form.Control type="text" placeholder="John Doe" required name="username" />
      </Form.Group>
      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" required name="password" />
      </Form.Group>
      <Form.Group controlId="confirm-password">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm Password"
          name="confirm-password"
          required
        />
      </Form.Group>
      <Form.Group className="form-btn-group">
        <Button className="form-btn" type="submit">
          Register
        </Button>
        <NavLink to="/" className="form-link">
          Login
        </NavLink>
      </Form.Group>
    </Form>
  );
};

const RegisterContent = () => {
  return <CardWrapper title="Register" children={<RegisterForm />} />;
};

export default RegisterContent;
