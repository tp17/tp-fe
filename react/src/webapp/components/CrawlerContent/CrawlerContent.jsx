import './CrawlerContent.scss';
import React, { useEffect, useRef } from 'react';
import CardWrapper from '../CardWrapper/CardWrapper';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { CrawlerService } from '../api/crawler-service';

const CrawlerParamForm = () => {
  const form = useRef(null);

  const handleCrawl = async (e) => {
    e.preventDefault();
    const data = new FormData(form.current);
    await CrawlerService.crawl(data.get('root-url'), data.get('max'));
  };

  return (
    <div>
      <Form onSubmit={handleCrawl} ref={form}>
        <Form.Group controlId="root-url">
          <Form.Row className="input-row">
            <Col className="label-col">
              <Form.Label className="form-label">Root URL</Form.Label>
            </Col>
            <Col>
              <Form.Control
                className="form-control"
                type="text"
                name="root-url"
                placeholder="https://fei.stuba.sk"
              />
            </Col>
          </Form.Row>
        </Form.Group>

        <Form.Group controlId="max">
          <Form.Row className="input-row">
            <Col className="label-col">
              <Form.Label className="form-label">Maximum</Form.Label>
            </Col>
            <Col>
              <Form.Control
                className="form-control"
                type="number"
                placeholder="5"
                name="max"
                required
                min="0"
                max="500"
                pattern="[0-9]{3}"
              />
            </Col>
          </Form.Row>
        </Form.Group>
        <Row className="justify-content-center">
          <Button className="form-btn" type="submit">
            Crawl
          </Button>
        </Row>
      </Form>
    </div>
  );
};

const CrawlerContent = () => {
  useEffect(() => {
    // CrawlerService.getCrawled(localStorage.getItem('username')).then(({ data }) => {
    //   console.log([...new Set(data.map((log) => log.session))]);
    // });
    // CrawlerService.crawl('https://www.fei.stuba.sk/').then();
  }, []);

  return <CardWrapper title="Crawl params" children={<CrawlerParamForm />} />;
};
export default CrawlerContent;
