import './SearchContent.scss';

import React, { useContext, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import SearchTable from '../SearchTable/SearchTable';
import { FileService } from '../api/routes';
import CardWrapper from '../CardWrapper/CardWrapper';
import userContext from '../contexts/userContext';
import { useHistory } from 'react-router-dom';
function SearchForm() {
  const [files, setFiles] = useState([]);
  const [zipFileIds, setZipFilesIds] = useState([]);
  const [data, setData] = useState('');
  const [showModal, setShowModal] = useState(false);
  const history = useHistory();

  const user = useContext(userContext);
  useEffect(() => {
    user.loggedIn ? FileService.getFilesFromDatabase(setFiles) : history.push('/');
  }, [user.loggedIn, history]);
  useEffect(() => {}, [zipFileIds, data]);

  const returnFile = (data) => {
    let blob = new Blob([data]);
    const downloadUrl = URL.createObjectURL(blob);
    let a = document.createElement('a');
    a.href = downloadUrl;
    a.download = `${Date.now()}.zip`;
    document.body.appendChild(a);
    a.click();
  };
  const handleSubmit = () => {
    let formData = new FormData();
    formData.append('ids', zipFileIds);
    FileService.uploadFilesToZip(formData).then((res) => returnFile(res.data));
  };
  if (!files) {
    return null;
  }
  return (
    <>
      {files.length > 0 && <SearchTable data={files} setFiles={setZipFilesIds} />}
      <Form.Group className="form-btn-group search">
        <Button className="form-btn" onClick={() => zipFileIds.length > 0 && handleSubmit()}>
          Download {zipFileIds.length}
        </Button>
      </Form.Group>
    </>
  );
}

const SearchContent = () => {
  return <CardWrapper title="Search" children={<SearchForm />} />;
};

export default SearchContent;
