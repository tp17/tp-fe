import './CardWrapper.scss';
import React from 'react';
import { Card } from 'react-bootstrap';

const CardWrapper = ({ children, title, className }) => {
  return (
    <Card className={`card-wrapper shadow ${className}`}>
      <Card.Body style={{ overflowY: 'auto' }}>
        {title && (
          <>
            <Card.Title>
              <h2>{title}</h2>
            </Card.Title>
            <hr />
          </>
        )}
        {children}
      </Card.Body>
    </Card>
  );
};

export default CardWrapper;
