import './Footer.scss';
import React from 'react';
import { Col, Row } from 'react-bootstrap';

const Footer = () => {
  return (
    <div className="footer-wrapper">
      <Row>
        <Col>Peter Hajduk</Col>
        <Col
          onClick={() =>
            window.open(
              'https://media.discordapp.net/attachments/757670993148903424/818424588948996096/unknown.png',
              '_blank'
            )
          }
        >
          Andrej Fula
        </Col>
        <Col
          onClick={() =>
            window.open(
              'https://instagram.fbts7-1.fna.fbcdn.net/v/t51.2885-15/e35/p1080x1080/122790141_437519020566554_4131475721044473192_n.jpg?tp=1&_nc_ht=instagram.fbts7-1.fna.fbcdn.net&_nc_cat=102&_nc_ohc=XeYJO16niv8AX_DThcY&ccb=7-4&oh=165fca84dc10fa6ccd0a7d77a0102397&oe=608B684B&_nc_sid=4f375e',
              '_blank'
            )
          }
        >
          Tomáš Zachar
        </Col>
        <Col>Filip Wagner</Col>
        <Col>Jakub Šíp</Col>
        <Col
          onClick={() =>
            window.open(
              'https://scontent-vie1-1.xx.fbcdn.net/v/t1.15752-9/167130189_1579769892217455_5429890509689390112_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=ae9488&_nc_ohc=juvhuM1beCMAX-A8526&_nc_ht=scontent-vie1-1.xx&oh=b6f74f4b39881955f7ad4b92f46102d8&oe=608F3B09',
              '_blank'
            )
          }
        >
          Matúš Straňák
        </Col>
      </Row>
      <Row>
        <Col>© 2021 Malware Boiis. All rights reserved.</Col>
      </Row>
    </div>
  );
};

export default Footer;
