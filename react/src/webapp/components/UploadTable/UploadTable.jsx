import './UploadTable.scss';
import momentDurationFormatSetup from 'moment-duration-format';
import moment from 'moment';
import React from 'react';
import { Spinner } from 'react-bootstrap';
import DefaultTable from '../DefaultTable/DefaultTable';

const UploadTable = ({ data }) => {
  momentDurationFormatSetup(moment);
  const now = moment();
  const tableConfig = {
    dense: true,
    striped: true,
    columns: [
      {
        name: 'File name',
        selector: 'filename',
        sortable: true,
        grow: 4,
      },
      {
        name: 'Start date',
        selector: 'started',
        sortable: true,
        grow: 3,
        format: (row) => `${moment(row.started).format('DD.MM.YYYY, H:mm:ss')}`,
      },
      {
        name: 'Runtime',
        selector: 'runtime',
        sortable: true,
        format: (row) => `${moment.duration(now.diff(row.started)).format('H:mm:ss')}`,
      },
      {
        name: 'Status',
        selector: 'status',
        sortable: false,
        cell: () => <Spinner animation="border" size="sm" className="spinner-color" />,
      },
    ],
  };
  return <DefaultTable data={data} config={tableConfig} />;
};

export default UploadTable;
