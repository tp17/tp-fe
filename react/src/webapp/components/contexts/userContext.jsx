import { createContext } from 'react';

export const INITIAL = {
  loggedIn: !!localStorage.getItem('key'),
  userName: localStorage.getItem('username'),
  userAPI: !localStorage.getItem('key'),
};
const userContext = createContext({
  loggedIn: false,
  userName: '',
  userAPI: '',
});

export default userContext;
