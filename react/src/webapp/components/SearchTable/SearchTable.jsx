import './SearchTable.scss';
import React from 'react';

import DefaultTable from '../DefaultTable/DefaultTable';

const SearchTable = ({ data, setFiles }) => {
  const addToArray = (e) => {
    setFiles(e.selectedRows.map((file) => file.id));
  };

  /**
   * Legacy of https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
   * @param bytes
   * @param decimals
   * @returns {string}
   */
  const getSizeUnit = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 b';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  };

  const tableConfig = {
    striped: true,
    dense: true,
    selectable: true,
    onSelect: addToArray,
    columns: [
      {
        name: 'File name',
        selector: 'filename',
        sortable: true,
        grow: 4,
      },
      {
        name: 'Size',
        selector: 'length',
        sortable: true,
        format: (row) => getSizeUnit(row.length),
      },
      {
        name: 'Malware percentage',
        selector: 'percentage',
        sortable: true,
        format: (row) => `${row.malware || '0'} %`,
      },
    ],
  };
  return <DefaultTable config={tableConfig} data={data} />;
};

export default SearchTable;
