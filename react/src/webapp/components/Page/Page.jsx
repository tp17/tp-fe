import './Page.scss';
import React from 'react';
import Toolbar from '../Navbar/Toolbar';
import Footer from '../Footer/Footer';
import { animated, useSpring } from 'react-spring';
import background from '../../static/images/main_background.jpeg';

const Page = ({ children }) => {
  const [props, set] = useSpring(() => ({
    xy: [0, 0],
    config: { mass: 10, tension: 550, friction: 140 },
  }));
  const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2];
  const trans1 = (x, y) => `translate3d(${x / 50}px,${y / 10}px,0)`;
  return (
    <div style={{ height: '100%', position: 'relative' }}>
      <Toolbar />
      <div style={{ height: '100%', position: 'relative' }}>
        <div className="form-wrapper">{children}</div>
        <div className="animated-img-wrapper">
          <animated.img
            src={background}
            className="background"
            alt="background"
            onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}
            style={{ transform: props.xy.interpolate(trans1), marginLeft: '-12px', width: '102%' }}
          />
        </div>
      </div>
      <Footer style={{ zIndex: 10 }} />
    </div>
  );
};

export default Page;
